from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout
from json_response import *
import datetime
from forms import *
from models import *
from errors import *
import os


def private_office(request, id):
    try:
        user = User.objects.get(id=id)
    except:
        return error(request, "Current user does not exists!")

    return render_to_response('profile/user_page.html', {
        "current_user":user,
    }, context_instance=RequestContext(request))

def my_info(request):
    if request.method == 'POST' and request.is_ajax():
        if request.GET['form'] == '0':
            ChangeUserInfoForm(request.POST, instance=request.user).save()
            return HttpResponse()
        elif request.GET['form'] == '1':
            ChangeUserContactInfoForm(request.POST, instance=request.user.profile).save()
            return HttpResponse()
        elif request.GET['form'] == '2':
            ChangeInfoAboutUserSkillsForm(request.POST, instance=request.user.profile).save()
            return HttpResponse()

    user_info_form = ChangeUserInfoForm(instance=request.user)
    user_want_study_form = ChangeInfoAboutUserSkillsForm(instance=request.user.profile)
    user_contact_info_form = ChangeUserContactInfoForm(instance=request.user.profile)

    return render_to_response('profile/change_private_info.html', {
        'user_info_form': user_info_form,
        'profile_info_form': user_contact_info_form,
        'user_want_study_form': user_want_study_form
    }, context_instance=RequestContext(request))

def update_avatar(request):
    print("cool")
    return HttpResponse()