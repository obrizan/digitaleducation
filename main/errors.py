from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout
import Image
import datetime
from forms import *
from models import *
import os

def error(request, error):
    return render_to_response('404.html', {
        "error":error
    }, context_instance=RequestContext(request))