from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout
import Image
import datetime
from forms import *
from models import *
from errors import *
import os


def create_course(request):
    if request.method == "POST":
        form = CourseForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            course = do_create_course(request, cd)
            return HttpResponseRedirect('/course' + str(course.id) + '/')
        else:
            return render_to_response('course/create_course.html', {
                    'form': form
                }, context_instance=RequestContext(request))
    form = CourseForm()
    return render_to_response('course/create_course.html', {
        'form': form
    }, context_instance=RequestContext(request))

def do_create_course(request, cd):
    course = Course.objects.create(creator=request.user,
                                   title=cd['title'],
                                   requirements=cd['requirements'],
                                   description=cd['description'],
                                   lectures_count=0,
                                   creation_date=datetime.datetime.now(),
                                   last_change_date=datetime.datetime.now())
    course.save()
    return course

def get_my_courses(request):
    user = request.user
    try:
        creator = Course.objects.filter(creator=user)
    except:
        creator = None
    try:
        course_listener = CoursesListeners.objects.filter(course_listener=user)
    except:
        course_listener = None

    return render_to_response('course/courses.html', {
        'creator': creator,
        'course_listener': course_listener
    }, context_instance=RequestContext(request))

def subscribe(request, course_id):
    course = Course.objects.get(id=course_id)
    user = request.user
    course.subscribe(user)
    return HttpResponseRedirect('/my_courses/')

def unsubscribe(request, course_id):
    course = Course.objects.get(id=course_id)
    user = request.user
    course.unsubscribe(user)
    return HttpResponseRedirect('/my_courses/')

def get_course(request, course_id):
    course = Course.objects.get(id=course_id)
    can_change = course.creator == request.user

    return render_to_response('course/course.html', {
        'course': course,
        'can_change': can_change
    }, context_instance=RequestContext(request))


def change_course_info(request, course_id):
    course = Course.objects.get(id=course_id)
    if course.creator == request.user:
        if request.method == "POST":
            form = CourseForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                do_change_course_info(course, cd)
                return HttpResponseRedirect('/course' + str(course.id) + '/')
            else:
                return render_to_response('course/course_info.html', {
                        'form': form
                    }, context_instance=RequestContext(request))
        form = CourseForm(initial={
            'description': course.description,
            'requirements': course.requirements,
            'title': course.title
        })

        return render_to_response('course/course_info.html', {
            'form': form,
            'course_id':course_id
        }, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/course' + str(course.id) + '/')

def do_change_course_info(course, cd):
    course.title = cd['title']
    course.requirements = cd['requirements']
    course.description = cd['description']
    course.save()


