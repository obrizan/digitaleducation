from django.db import models
from django.contrib.auth.models import User
import os


AVATAR_FOLDER = os.path.join(os.path.dirname(__file__), '../media/avatars').replace('\\','/')
AVATAR_FOLDER_ORIGINAL = os.path.join(os.path.dirname(__file__), '../media/avatars_original').replace('\\','/')
LECTURES_FOLDER = os.path.join(os.path.dirname(__file__), '../media/courses').replace('\\','/')

class Country(models.Model):
    name = models.CharField(max_length=20, default='', null=False, verbose_name='country')

class Avatar(models.Model):
    user = models.OneToOneField(User, unique=True, verbose_name='user')
    original = models.ImageField(upload_to=AVATAR_FOLDER_ORIGINAL, verbose_name='original avatar')
    thumbnail_big = models.ImageField(upload_to=AVATAR_FOLDER_ORIGINAL, verbose_name='big avatar thumbnail')
    thumbnail_small = models.ImageField(upload_to=AVATAR_FOLDER_ORIGINAL, verbose_name='small avatar thumbnail')


class Profile(models.Model):
    user = models.OneToOneField(User, unique=True, verbose_name='user')
    phone = models.CharField(max_length=20, default='', null=True, verbose_name='phone')
    mobile_phone = models.CharField(max_length=20, default='', null=True, verbose_name='mobile phone')
    skills = models.TextField(max_length=255, default='', null=True, verbose_name='user skills')
    want_study = models.TextField(max_length=255, default='', null=True, verbose_name='want study')
    skype = models.CharField(max_length=50, default='', null=True, verbose_name='skype')

    def __unicode__(self):
        return str(self.id) + self.user.username


class Course(models.Model):
    creator = models.ForeignKey(User, unique=False, null=False, verbose_name='creator')
    title = models.CharField(max_length=100, default='', null=False, unique=False, verbose_name='course title')
    requirements = models.TextField(max_length=400, default='', null=True, unique=False, verbose_name='requirements')
    description = models.TextField(max_length=400, default='', null=False, unique=False, verbose_name='description')
    lectures_count = models.IntegerField(default=0, null=True, unique=False, verbose_name='lectures count')
    creation_date = models.DateField(auto_created=True)
    last_change_date = models.DateField()


class Lecture(models.Model):
    course = models.ForeignKey(Course, unique=False, null=False)
    title = models.CharField(max_length=100, null=False, unique=True)
    text = models.FileField(upload_to=LECTURES_FOLDER)
    file_name = models.CharField(max_length=50, default='')
    description = models.CharField(max_length=1000, null=True, unique=False)
    creation_date = models.DateField()
    last_change_date = models.DateField()

class CoursesListeners(models.Model):
    course = models.ForeignKey(Course, unique=False, null=False)
    listener = models.ForeignKey(User, unique=False, null=False)

class UsersSubscribers(models.Model):
    user = models.ForeignKey(User, unique=False, null=False, related_name="signed_user")
    subscriber = models.ForeignKey(User, unique=False, null=False, related_name="subscribed_user")