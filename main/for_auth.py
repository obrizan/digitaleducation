from main.views import *


def for_auth(view):
    def new_view(request, *args, **kwargs):
        if (request.user.is_authenticated()):
            return view(request, *args, **kwargs)
        else:
            return HttpResponseRedirect('/login/')
    return new_view