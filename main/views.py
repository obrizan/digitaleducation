from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout
import Image
import datetime
from forms import *
from models import *
import os


#need more refactoring
def my_registration(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=True)
            user.set_password(form.data['password'])
            user.save()
            Profile.objects.create(user=user).save()
            return HttpResponseRedirect('/login/')
        else:
            return render_to_response('profile/registration.html', {
                    'form': form
                }, context_instance=RequestContext(request))
    form = RegistrationForm()
    return render_to_response('profile/registration.html', {
        'form': form
    }, context_instance=RequestContext(request))


##########################################################################################################
#need small refactoring (class view)
def my_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.cleaned_data
            login(request, user)
            return HttpResponseRedirect('/id%s/'%str(user.id))
        else:
            return render_to_response('profile/login.html', {
                    'form': form
                }, context_instance=RequestContext(request))
    if request.method == "GET":
        if request.user.is_active:
            return HttpResponseRedirect('/id%s/'%str(request.user.id))
    form = LoginForm()
    return render_to_response('profile/login.html', {
                    'form': form
                }, context_instance=RequestContext(request))

def my_logout(request):
    logout(request)
    return HttpResponseRedirect('/')