from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout
import Image
import datetime
from forms import *
from models import *
from errors import *
import os

def add_lecture(request, course_id):
    if Course.objects.get(id=course_id).creator == request.user:
        if request.method == "POST":
            form = LectureForm(request.POST, request.FILES)
            if form.is_valid():
                cd = form.cleaned_data
                do_add_lecture(request, cd, course_id)
                return HttpResponseRedirect('/course' + str(course_id) + '/')
            else:
                return render_to_response('lecture/create_lecture.html', {
                        'form': form,
                        'course':course_id
                    }, context_instance=RequestContext(request))
        form = LectureForm()
        return render_to_response('lecture/create_lecture.html', {
            'form': form,
            'course':course_id
        }, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/course' + str(course_id) + '/')

def do_add_lecture(request, cd, course_id):
    lecture = Lecture.objects.create(course=Course.objects.get(id=course_id),
                                   title=cd['title'],
                                   description=cd['description'],
                                   creation_date=datetime.datetime.now(),
                                   last_change_date=datetime.datetime.now())

    name = str(course_id) + '_' + str(lecture.id) + ".pdf"
    cd['text'].name = name
    lecture.text = cd['text']
    lecture.file_name = name
    lecture.save()

def change_lecture_info(request, course_id, lecture_id):
    course = Course.objects.get(id=course_id)
    lecture = Lecture.objects.get(id=lecture_id)
    if course.creator == request.user:
        if request.method == "POST":
            form = LectureForm(request.POST, request.FILES)
            if form.is_valid():
                cd = form.cleaned_data
                do_change_lecture_info(lecture, course_id, cd)
                return HttpResponseRedirect('/course' + str(course_id) + '/')
            else:
                return render_to_response('lecture/lecture_info.html', {
                        'form': form,
                        'course':course_id,
                        'lecture':lecture_id
                    }, context_instance=RequestContext(request))
        form = LectureForm(initial={
            'description': course.description,
            'title': course.title
        })

        return render_to_response('lecture/lecture_info.html', {
            'form': form,
            'course':course_id,
            'lecture':lecture_id
        }, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/course' + str(course.id) + '/')

def do_change_lecture_info(lecture, course_id, cd):
    if cd['text'] is not None:
        os.remove(LECTURES_FOLDER + "/" + str(course_id) + "_" + str(lecture.id) + '.pdf')
        name = str(course_id) + '_' + str(lecture.id) + ".pdf"
        cd['text'].name = name
        lecture.text = cd['text']
        lecture.file_name = name
        lecture.save()

    lecture.title = cd['title']
    lecture.description = cd['description']
    lecture.save()

def get_lecture(request, course_id, lecture_id):
    course = Course.objects.get(id=course_id)
    lecture = Lecture.objects.get(id=lecture_id)
    if course.creator == request.user:
        return render_to_response('lecture/lecture.html', {
            'course':course_id,
            'lecture':lecture
        }, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/course' + str(course.id) + '/')