from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from main.views import *
from main.for_auth import *
from main.profile import *
from search import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    (r'^$', my_login),
    (r'^registration/$', my_registration),
    (r'^login/$', my_login),
    (r'^[\w*/]*logout/$', login_required(my_logout)),
    (r'^id(?P<id>\d+)/$', login_required(private_office)),
    (r'^my_info/$', login_required(my_info)),
    (r'^search/$', login_required(search)),
    (r'^update_avatar/$', login_required(update_avatar)),
    #(r'^change_avatar/$', for_auth(change_original_avatar)),
    #(r'^create_course/$', for_auth(create_course)),
    #(r'^my_courses/$', for_auth(get_my_courses)),
    #(r'^my_info/$', for_auth(private_info)),
    #(r'^search/$', for_auth(search)),
    #(r'^create_course/$', for_auth(create_course)),
    #(r'^course(?P<course_id>\d+)/$', for_auth(get_course)),
    #(r'^course(?P<course_id>\d+)/change_info/$', for_auth(change_course_info)),
    #(r'^course(?P<course_id>\d+)/add_lecture/$', for_auth(add_lecture)),
    #(r'^course(?P<course_id>\d+)/lecture(?P<lecture_id>\d+)/$', for_auth(get_lecture)),
    #(r'^course(?P<course_id>\d+)/lecture(?P<lecture_id>\d+)/change/$', for_auth(change_lecture_info)),
)
