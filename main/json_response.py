import simplejson
from django.core import serializers
from django.http import HttpResponse


def HttpResponseJson(data=''):
    data = serializers.serialize('json', data, fields=('first_name', 'last_name', 'phone'))
    response=simplejson.dumps({'data':data})
    return HttpResponse(response,content_type='application/json')


def HttpResponseJsonHtml(status,message='',data=''):
    response=simplejson.dumps({
                                    'status': status,
                                    'message': message,
                                    'data': data
                                    })
    return HttpResponse(response)


def HttpResponseIncorrectData():
    return HttpResponseJson('fail', 'Incorrect Data')