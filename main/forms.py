from distutils.command.clean import clean
from django.contrib.auth import authenticate
from django.forms import *
from models import *


class RegistrationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)

        self.fields['username'].help_text = None

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'email',
            'password'
        ]
        widgets = {
            'password': PasswordInput,
            'email': TextInput
        }

    repeat_password = CharField(max_length=30, min_length=5, widget=PasswordInput( attrs={ 'class':'form-control' } ))


class LoginForm(Form):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        self.fields['username'].help_text = None

    username = CharField(max_length=30, min_length=5,widget=TextInput( attrs={ 'class':'form-control' } ))
    password = CharField(max_length=30, min_length=5, widget=PasswordInput( attrs={ 'class':'form-control' } ))

    def clean(self):
        super(LoginForm, self).clean()
        cd = self.cleaned_data
        user = authenticate(username=cd['username'], password=cd['password'])
        if user is None:
            raise ValidationError("Wrong username or password!")
        return user

class ChangeUserInfoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ChangeUserInfoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name'
        ]
        widgets = {
        }

class ChangeInfoAboutUserSkillsForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ChangeInfoAboutUserSkillsForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Profile
        fields = [
            'skills',
            'want_study'
        ]
        widgets = {
        }

class ChangeUserContactInfoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ChangeUserContactInfoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Profile
        fields = [
            'phone',
            'mobile_phone',
            'skype'
        ]
        widgets = {
        }

class SearchUserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SearchUserForm, self).__init__(*args, **kwargs)

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email'
        ]
        widgets = {
        }

class SearchProfileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SearchProfileForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Profile
        fields = [
            'phone',
            'mobile_phone',
            'skype',
            'skills',
            'want_study'
        ]
        widgets = {
            'skills':TextInput(),
            'want_study':TextInput()
        }