from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q
import Image
import datetime
from forms import *
from models import *
from errors import *
from json_response import *
import os

def search(request):
    if request.method == "POST" and request.is_ajax():
        res_dict = request.POST
        if(res_dict['first_name'] or res_dict["last_name"]):
            result = User.objects.filter(Q(first_name__icontains=res_dict['first_name']) &
                                         Q(last_name__icontains=res_dict['last_name']))
        else:
            result = User.objects.all()

        result = result.filter(email__icontains=res_dict['email'])
        result = result.filter(profile__phone__icontains=res_dict['phone'])
        result = result.filter(profile__mobile_phone__icontains=res_dict['mobile_phone'])
        result = result.filter(profile__skype__icontains=res_dict['skype'])
        result = result.filter(profile__want_study__icontains=res_dict['want_study'])
        result = result.filter(profile__skills__icontains=res_dict['skills'])
        return HttpResponseJson(data=result)

    search_user_form = SearchUserForm()
    search_profile_form = SearchProfileForm()

    return render_to_response('search/search.html', {
                    'search_user_form': search_user_form,
                    'search_profile_form': search_profile_form,
                }, context_instance=RequestContext(request))

