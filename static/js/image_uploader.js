/**
 * Created with PyCharm.
 * User: yura
 * Date: 3/10/15
 * Time: 5:11 PM
 * To change this template use File | Settings | File Templates.
 */
$("div.Avatar").hover(function(){
    $(this).children("div").fadeIn(600);
},function(){
$(this).children("div").fadeOut(200);});

var resized_selection_cords;
var resized_image;

function ErrorMessage(error_div, message)
{
    error_div.addClass('div-error');
    error_div.html(message);
}

function HideErrorMessage(error_div)
{
    error_div.removeClass('div-error');
    error_div.empty();
}

function CreatePopUpDiv()
{
    $("div.Avatar div").fadeOut(1);
    $("div.pop-up-wrapper").fadeIn(100);
}

function ClosePopUpDiv()
{
    location.reload();
}

function GetImage(url)
{
    var image = new Image();
    image.src = url;
    return image;
}

function ResizePreviewDiv(img, image)
{
    if(image.width > image.height)
    {
        img.attr('max-width', '500px');
        img.attr('min-height', '100px');
    }
    else
    {
        img.attr('max-height', '500px');
        img.attr('min-width', '100px');
    }
}

function ShowUploadedImagePreview()
{
    var input = $("div.pop-up-div-form-input input")[0];
    if(input.files && input.files[0])
    {
        if(input.files[0].type.match('image.*'))
        {
            var reader = new FileReader();
            reader.onload = function(e)
            {
                var image = GetImage(e.target.result);
                if(image.width < 200 || image.height < 250)
                    ErrorMessage($("div.pop-up-div-error"),"Image size must be more than 250x200");
                else
                {
                    var img = $(".pop-up-div-form .avatar-preview img");
                    $(".pop-up-div").css('margin-top', '50px');
                    HideErrorMessage($("div.pop-up-div-error"));
                    ResizePreviewDiv(img, image);
                    img.attr('src', e.target.result).imgAreaSelect({
                        aspectRatio: '3:4',
                        x1: 0,
                        y1: 0,
                        x2: 200,
                        y2: 267,
                        minWidth: 200,
                        minHeight: 250,
                        onSelectChange: function(img, selection)
                        {
                            resized_image = img;
                            resized_selection_cords = { 'x1': selection.x1,
                                                        'y1': selection.y1,
                                                        'x2': selection.x2,
                                                        'y2': selection.y2 };
                        },
                        handles: true});
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
}

function SendData()
{
        $.ajax({ url: "/update_avatar/",
                 type: 'post',
                 data: {
                    // img: resized_image,
                    // cords: resized_selection_cords
                 },
                 success:function(result){
                     alert(result) }});
}

